//
//  SignatureViewController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class SignatureViewController: BaseViewController,YPSignatureDelegate {
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    var user_id = ""
    var order_id = ""
    var shipment_id = ""
    var feedbackValue=""
    var feedbackText=""
    var date = Date()
    var image=""
   
    @IBAction func clearSign(_ sender: UIButton) {
        self.signatureView.clear()
    }
   
    func didStart() {
    }
    
    func didFinish() {
    }
    override func viewDidLoad() {
        super.viewDidLoad() 
        
        save.addTarget(self, action: #selector(savePressed(_:)), for: .touchUpInside)
        signatureView.backgroundColor=UIColor(hexString: "#d3d3d3")
        signatureView.layer.cornerRadius=7
        signatureView.layer.borderWidth=1
        signatureView.layer.borderColor=UIColor.black.cgColor
        user_id = UserDefaults.standard.value(forKey: "mageid") as! String
        signatureView.delegate=self
        // Do any additional setup after loading the view.
    }
    
    @objc func savePressed(_ sender:UIButton)
    {
       
        if let signatureImage = self.signatureView.getSignature(scale: 10) {
            
            self.signatureView.clear()
            let imagedataBase64 = signatureImage.jpegData(compressionQuality: 0.5)!.base64EncodedString()
            let imageData = ["type":"file","name":order_id+".jpeg","base64_encoded_data":imagedataBase64] as [String : String]
            
            let postString = ["user_id":user_id,"order_id":order_id,"comment":feedbackText,"rating":feedbackValue,"shipment_id":shipment_id,"image":imageData.convtToJson()] as [String : Any]
            self.sendRequest(url: "mobidelivery/customer/savefeedback", params: postString as! Dictionary<String, String>)
        }
        
    }
    
  
    

    
  
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        do{
            if let json = try? JSON(data:data!)
            {
                let json = json[0]
                if requestUrl == "mobidelivery/customer/otpverification"{
                    print(json)
                   
                }else {
                    print(json["data"]["customer"])
                    if json["data"]["customer"][0]["status"]=="success"
                    {
                        print("truetruetrue")
                        self.view.showToastMsg(json[0]["data"]["customer"][0]["message"].stringValue)
                        let vc=UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderNavigate") as! OrderNavigationController
                        self.present(vc, animated: true, completion: nil)
                    }
                }
                
             
            }
        }
        catch let error
        {
            print("!!!***@####")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
