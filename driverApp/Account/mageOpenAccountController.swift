//
//  mageOpenAccountController.swift
//  MageNativeOpenCart
//
//  Created by cedcoss on 10/05/18.
//  Copyright © 2018 MageNative. All rights reserved.
//

import UIKit

class mageOpenAccountController: BaseViewController,UITableViewDelegate,UITableViewDataSource  {

    @IBOutlet weak var accountTableView: UITableView!

    var options         = [["Transaction","Pay Request List","Sign out"]]
    var userData        = [String:String]()
    var languages       : languageData?
    var onlineSwitch    = UISwitch()

    override func viewDidLoad() {
        super.viewDidLoad()
        accountTableView.delegate = self
        accountTableView.dataSource = self
        accountTableView.tableFooterView = UIView()
        
        
        if !appData.displayFee {
           options = [["Sign out"]]
        }
        
        
        if let userId   = UserDefaults.standard.value(forKey: "mageid") as? String {
            let params  = ["driver_id":userId]
            self.sendRequest(url: "mobidelivery/customer/driverprofile", params: params)
        }
    }

    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data:data)
            print(json)
            json = json[0]
            if requestUrl == "mobidelivery/customer/driverstatus" {
                print(json)
                if let cell = accountTableView.dequeueReusableCell(withIdentifier: "profileImage") as? mageOpenHeaderCell {
                if json["success"].stringValue == "true"{
                        print(cell.onlineSwitch.isOn)
                        if cell.onlineSwitch.isOn {
                            cell.status.text = "Offline"
                           //self.onlineSwitch.setOn(true, animated: true)
                        }else {
                            cell.status.text = "Online"
                           //self.onlineSwitch.setOn(false, animated: true)
                        }
                }else {
                    if userData["status"] == "1"{
                        self.onlineSwitch.setOn(true, animated: true)
                    }else{
                        self.onlineSwitch.setOn(false, animated: true)
                    }
                    self.view.showToastMsg(json["message"].stringValue )
                    }
                }
            }else {
            if json["success"].stringValue == "true" {
                userData["name"] = json["name"].stringValue
                userData["email"] = json["email"].stringValue
                userData["status"] = json["status"].stringValue
                let cell = accountTableView.dequeueReusableCell(withIdentifier: "profileImage") as? mageOpenHeaderCell
                self.onlineSwitch = cell?.onlineSwitch ?? UISwitch()
                if  let name = userData["name"]{
                    let charc = (name.first?.description)!
                    let attr = [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 15)]
                            let welcomText = NSMutableAttributedString()
                            welcomText.append(NSAttributedString(string: "Hi,\n"))
                            let nameString = NSAttributedString(string: name.uppercased(), attributes: attr)
                            welcomText.append(nameString)
                    let attr1 = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14)]
                        let nameString1 = NSAttributedString(string:  "\n"+userData["email"]! , attributes: attr1)
                            welcomText.append(nameString1)
                        cell?.nameLabel.numberOfLines = 0
                        cell?.nameLabel.attributedText = welcomText
                        cell?.onlineSwitch.addTarget(self, action: #selector(self.setSwitchValue), for: .valueChanged)
                        cell?.initials.text = charc
                        cell?.initials.textColor = .white
                }
                        switch  userData["status"] {
                        case "1":
                            cell?.onlineSwitch.setOn(true, animated: true)
                            cell?.status.text  = NSLocalizedString("Online", comment: "")
                        default:
                            cell?.status.text  = NSLocalizedString("Offline", comment: "")
                            cell?.onlineSwitch.setOn(false, animated: true)
                        }
                accountTableView.tableHeaderView = cell
                self.accountTableView.reloadData()
            }else{
                print("Hello")
                }
            }
        }catch let error {
            print(error.localizedDescription)
        }
    }
    
    @objc func setSwitchValue(sender:UISwitch){
        if let userId = UserDefaults.standard.value(forKey: "mageid") as? String {
            var params = ["driver_id":userId]
        if sender.isOn {
            params["driver_status"] = "1"
        }else {
             params["driver_status"] = "0"
        }
        self.sendRequest(url: "mobidelivery/customer/driverstatus", params: params)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func logoutButtonPressed()
    {
        let userId = UserDefaults.standard.value(forKey: "mageid") as? String ?? ""
        let param = ["driver_id":userId,"driver_status":"0","has_logged_out":"true"]
        self.sendRequest(url: "mobidelivery/customer/driverstatus", params: param)
        
        UserDefaults.standard.removeObject(forKey: "isLogin")
        UserDefaults.standard.removeObject(forKey: "mageid")
        //UserDefaults.standard.removeObject(forKey: "checkloginstatus")
        UserDefaults.standard.removeObject(forKey: "mageusername")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewControl = storyboard.instantiateViewController(withIdentifier: "login") as? LoginViewController
        viewControl?.modalPresentationStyle = .fullScreen
        self.present(viewControl!, animated: true, completion: nil)
    }
}

extension mageOpenAccountController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options[section].count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellData", for: indexPath) as? mageOpenMyAccountCell {
            cell.titleLabel.text = NSLocalizedString(options[indexPath.section][indexPath.row], comment: "")//
            cell.accountImage.image = UIImage(named: options[indexPath.section][indexPath.row])
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
            
        default:
            return nil
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option = options[indexPath.section][indexPath.row]
        if option == "Sign out"{
            self.logoutButtonPressed()
        }

        switch option{
        case "Sign out":
            self.logoutButtonPressed()
        case "Transaction":
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TransactionListingVC") as! TransactionListingVC
            self.navigationController?.pushViewController(vc, animated: true)
        case "Pay Request List":
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PayRequestListingVC") as! PayRequestListingVC
        self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("Default")
        }
    }
}

struct languageData:Decodable{
    let languages:[language]?
    let status:String?
}
struct language:Decodable {
    let language_id:String?
    let name:String?
    let code:String?
    let locale:String?
    let image:String?
    let directory:String?
    let sort_order:String?
    let status:String?
}

