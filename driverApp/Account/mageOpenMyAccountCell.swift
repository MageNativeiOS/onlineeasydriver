//
//  mageOpenMyAccountCell.swift
//  MageNativeOpenCart
//
//  Created by cedcoss on 09/06/18.
//  Copyright © 2018 MageNative. All rights reserved.
//

import UIKit

class mageOpenMyAccountCell: UITableViewCell {

    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
