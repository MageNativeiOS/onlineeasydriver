//
//  mageOpenHeaderCell.swift
//  driverApp
//
//  Created by cedcoss on 25/12/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class mageOpenHeaderCell: UITableViewCell {

    @IBOutlet weak var initials: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var onlineSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
