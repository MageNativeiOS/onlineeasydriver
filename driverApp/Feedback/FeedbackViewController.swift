//
//  FeedbackViewController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class FeedbackViewController: BaseViewController {

    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var feedbackTextView: UITextField!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn8: UIButton!
    @IBOutlet weak var btn9: UIButton!
    @IBOutlet weak var btn10: UIButton!
    
    @IBOutlet weak var signatureBtn: UIButton!
    var popover:Popover?
    var feedbackValue=""
    var order_id = ""
    var shipment_id = ""
    var  user_id = UserDefaults.standard.value(forKey: "mageid") as! String
    var showPop = String()
    var topText = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        orderId.text=topText
        
        feedbackTextView.layer.borderColor=UIColor.black.cgColor
        feedbackTextView.layer.borderWidth=CGFloat(1)
        feedbackTextView.layer.cornerRadius=CGFloat(7)
        
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
        btn1.addTarget(self, action: #selector(btn1Pressed(_:)), for: .touchUpInside)
        btn2.addTarget(self, action: #selector(btn2Pressed(_:)), for: .touchUpInside)
        btn3.addTarget(self, action: #selector(btn3Pressed(_:)), for: .touchUpInside)
        btn4.addTarget(self, action: #selector(btn4Pressed(_:)), for: .touchUpInside)
        btn5.addTarget(self, action: #selector(btn5Pressed(_:)), for: .touchUpInside)
        btn6.addTarget(self, action: #selector(btn6Pressed(_:)), for: .touchUpInside)
        btn7.addTarget(self, action: #selector(btn7Pressed(_:)), for: .touchUpInside)
        btn8.addTarget(self, action: #selector(btn8Pressed(_:)), for: .touchUpInside)
        btn9.addTarget(self, action: #selector(btn9Pressed(_:)), for: .touchUpInside)
        btn10.addTarget(self, action: #selector(btn10Pressed(_:)), for: .touchUpInside)
        
        signatureBtn.addTarget(self, action: #selector(signatureBtnPressed(_:)), for: .touchUpInside)
        signatureBtn.backgroundColor=UIColor(hexString: appData.themeColor2)
        signatureBtn.setTitleColor(UIColor.white, for: .normal)
        signatureBtn.layer.cornerRadius = 5

        setDoneOnKeyboard()
        /*if showPop.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() != "false".lowercased(){
        self.showPopup()
        }*/
        // Do any additional setup after loading the view.
    }
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.feedbackTextView.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func btn1Pressed(_ sender:UIButton)
    {
        feedbackValue="1"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
 
    }
    @objc func btn2Pressed(_ sender:UIButton)
    {
        feedbackValue="2"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn3Pressed(_ sender:UIButton)
    {
        feedbackValue="3"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn4Pressed(_ sender:UIButton)
    {
        feedbackValue="4"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn5Pressed(_ sender:UIButton)
    {
        feedbackValue="5"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn6Pressed(_ sender:UIButton)
    {
        feedbackValue="6"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn7Pressed(_ sender:UIButton)
    {
        feedbackValue="7"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn8Pressed(_ sender:UIButton)
    {
        feedbackValue="8"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn9Pressed(_ sender:UIButton)
    {
        feedbackValue="9"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn1.backgroundColor=UIColor(hexString: "#999999")
        btn10.backgroundColor=UIColor(hexString: "CCFF33")
    }
    @objc func btn10Pressed(_ sender:UIButton)
    {
        feedbackValue="10"
        sender.backgroundColor=UIColor.white
        //sender.setTitleColor(UIColor.black, for: .normal)
        btn2.backgroundColor=UIColor(hexString: "#999999")
        btn3.backgroundColor=UIColor(hexString: "#999999")
        btn4.backgroundColor=UIColor(hexString: "#999999")
        btn5.backgroundColor=UIColor(hexString: "#999999")
        btn6.backgroundColor=UIColor(hexString: "FF9900")
        btn7.backgroundColor=UIColor(hexString: "FF9900")
        btn8.backgroundColor=UIColor(hexString: "FF9900")
        btn9.backgroundColor=UIColor(hexString: "CCFF33")
        btn1.backgroundColor=UIColor(hexString: "#999999")
    }
    @objc func signatureBtnPressed(_ sender:UIButton)
    {
        print(feedbackValue)
       if feedbackTextView.text=="" || feedbackValue==""
        {
            self.view.showToastMsg("Please fill your valuable feedback")
        }
        if feedbackValue != "" && feedbackTextView.text != ""
        {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sign")as! SignatureViewController
            vc.shipment_id = shipment_id
            vc.order_id = order_id
            vc.feedbackValue=feedbackValue
            vc.feedbackText=feedbackTextView.text!
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showPopup(){
        let otpView = mageOtpverification(frame: CGRect(x: 0, y: self.view.frame.origin.y + 10, width: self.view.frame.width-20, height: 200))
        otpView.otpText.tag = 1132
        otpView.submitButton.addTarget(self, action: #selector(self.submitFeedBack(sender:)), for: .touchUpInside)
        
        let popoverOptions: [PopoverOption] = [
            .type(.down),
            .arrowSize(CGSize(width: 25.0, height: 20.0)),
            .dismissOnBlackOverlayTap(false)
            
        ]
        
        self.popover?.tag = 2342
        self.popover = Popover(options: popoverOptions)
        self.popover?.showAsDialog(otpView)
      
        
    }

    @objc  func submitFeedBack(sender:UIButton){
        
        if let textView = self.popover?.viewWithTag(1132) as? UITextField {
            let text = textView.text
            let params = ["driver_id":user_id,"order_id":order_id,"otp":text,"shipment_id":shipment_id]
            self.sendRequest(url: "mobidelivery/customer/otpverification", params:params as! Dictionary<String, String> )
        }
    }

  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            do {
            var json = try JSON(data:data)
                json = json[0]
            print(json)
                if json["success"].stringValue == "true"{
                    self.popover?.dismiss()
                }else {
                    self.view.showToastMsg(json["message"].stringValue)
                }
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }

}
