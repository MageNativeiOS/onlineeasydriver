//
//  googleMapViewController.swift
//  driverApp
//
//  Created by cedcoss on 21/11/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit
import CoreLocation

class googleMapViewController: BaseViewController,CLLocationManagerDelegate,GMSMapViewDelegate {

    var manager:CLLocationManager!

    @IBOutlet weak var mapView: GMSMapView!
    var address = String()
    var urLocation = CLLocationCoordinate2D()
    let yourPoint = GMSMarker()
    var locationMarked = true
    var destination = CLLocationCoordinate2D()
    @IBOutlet weak var openGoogleMap: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
          manager = CLLocationManager()
        mapView.delegate = self
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getUserLocation()
    }
    
   
    @IBAction func openGoogleMap(_ sender: Any) {
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.open(URL(string:
                    "comgooglemaps://?saddr=&daddr=\(destination.latitude),\(destination.longitude)&directionsmode=driving")!)
            } else {
                NSLog("Can't use comgooglemaps://");
            }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)

        if let cordinates = manager.location?.coordinate {
            yourPoint.position = cordinates
            urLocation = cordinates
            if locationMarked {
            self.setDataToMap(cordinates: cordinates)
            }
        }
        print("heloooodkhsk")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
          manager.requestWhenInUseAuthorization()
        } else if (status == CLAuthorizationStatus.authorizedAlways) {
            if let cordinates = manager.location?.coordinate {
                urLocation = cordinates
                 self.setDataToMap(cordinates: cordinates)
            }
        }
    }
    
    func getUserLocation(){
        print(urLocation)
         if let cordinates = manager.location?.coordinate {
            urLocation = cordinates
        }
        manager.allowsBackgroundLocationUpdates = true
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            if let cordinates = manager.location?.coordinate {
                urLocation = cordinates
                
                //   showRouteOnMap(userCodinates: point.position)
                DispatchQueue.main.async {
                    // self.getPolylineRoute(from: cordinates, to: self.coordinates)
                   self.setDataToMap(cordinates: cordinates)
                   
                }
            }
            
        }else {
            manager.requestWhenInUseAuthorization()
        }
       
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
    
    func setDataToMap(cordinates:CLLocationCoordinate2D){
        self.yourPoint.position = cordinates
        self.yourPoint.title = "Your Location"
        print(cordinates)
        self.yourPoint.map = self.mapView
        self.setAddressInMap(address: self.address)
    }
    
    
    func setAddressInMap(address: String){
        
        var baseURl = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&key="+google_Api_Key
        baseURl =    baseURl.addingPercentEncoding (withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let request = URLRequest(url: NSURL(string: baseURl)! as URL)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {
            data, response, error in
            guard error == nil && data != nil else
            {
                print("error=\(String(describing: error))")
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                return;
            }
            do {
                let json = try JSON(data: data!)
                print("Response from google API======",json)
                DispatchQueue.main.async
                    {
                        if(json["status"].stringValue.lowercased() == "OK".lowercased()){
                            let addresslat = json["results"][0]["geometry"]["location"]["lat"].stringValue
                            let addresslog = json["results"][0]["geometry"]["location"]["lng"].stringValue
                            let coordinate = CLLocationCoordinate2D(latitude: Double(addresslat)!, longitude: Double(addresslog)!)
                            let point = GMSMarker()
                            point.position = coordinate
                            point.title = "Delivery Location"
                            print(coordinate)
                            point.map = self.mapView
                            self.destination = coordinate
                            self.getPolylineRoute(from: self.urLocation, to: coordinate)
                        }else {
                            self.showAlert(title: "Error", msg: "Unable to track route.")
                        }
                        
                }
            }catch let error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        let coordinate0 = CLLocation(latitude: 5.0, longitude: 5.0)
        let from = CLLocation(latitude: source.latitude, longitude: source.longitude)
        let to = CLLocation(latitude: destination.latitude, longitude: destination.longitude)
        print("Distance from Destination===",from.distance(from: to))


        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key="+google_Api_Key)!
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        print(json)
                        guard let routes = json["routes"] as? NSArray else {
                            return
                        }
                        
                        if (routes.count > 0) {
                            let overview_polyline = routes[0] as? NSDictionary
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                            let points = dictPolyline?.object(forKey: "points") as? String
                            DispatchQueue.main.async {
                                self.locationMarked = false
                                self.showPath(polyStr: points!)
                                let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets.init(top: 170, left: 30, bottom: 30, right: 30))
                                self.mapView?.moveCamera(update)
                            }
                        }else {
                               DispatchQueue.main.async {
                            self.locationMarked = false
                            self.showAlert(title: "Error", msg: "Unable to draw route on map.")
                            }
                        }
                        
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    
                }
            }
        })
        task.resume()
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.strokeColor = UIColor.blue
        polyline.map = mapView
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
