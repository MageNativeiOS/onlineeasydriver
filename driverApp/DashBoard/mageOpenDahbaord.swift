//
//  mageOpenDahbaord.swift
//  driverApp
//
//  Created by cedcoss on 21/12/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class mageOpenDahbaord: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var weekData = [Double]()
    var weekAxis = [String]()
    var yearData = [Double]()
    var yearAxis = [String]()
    var dayData = [Double]()
    var dayAxis = [String]()
    var monthData = [Double]()
    var monthAxis = [String]()
    var MsgCount = [String:String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        if let _ = UserDefaults.standard.object(forKey: "pending"){
            UserDefaults.standard.removeObject(forKey: "pending")
            if let viewController =  storyboard?.instantiateViewController(withIdentifier: "orderPage") as? OrderDetailViewController {
                viewController.status = "assigned"
                viewController.titel = "Pending Orders"
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let userId = UserDefaults.standard.value(forKey: "mageid") as? String {
            self.sendRequest(url: "mobidelivery/customer/dashboard/index", params: ["driver_id":userId])
            self.sendRequest(url: "mobidelivery/customer/notification", params: ["driver_id":userId])
        }
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data: data)
            json = json[0]
            print(json)
            if requestUrl == "mobidelivery/customer/dashboard/index" {
            for (key,val) in json["data"]["dashboard_chart"]
            {
                let data = val["data"].arrayValue
                print(data)
                let Xaxis = val["xaxis"].arrayValue
                switch key {
                case "week":
                    var temp = [Double]()
                    
                    for week in data{
                        print(week)
                        let data = Double(Int(week["value"].stringValue)!)
                        temp.append(data)
                    }
                    weekData = temp
                    var temp1 = [String]()
                    for axis in Xaxis{
                        let data = axis["value"].stringValue
                        temp1.append(data)
                    }
                    weekAxis = temp1
                case "day":
                    if dayData.count > 0 {
                        dayData.removeAll()
                    }
                    for day in data{
                        let data = Double(Int(day["value"].stringValue)!)
                        dayData.append(data)
                    }
                    if dayAxis.count > 0 {
                        dayAxis.removeAll()
                    }
                    for axis in Xaxis{
                        let data = axis["value"].stringValue
                        dayAxis.append(data)
                    }
                case "year":
                    if yearData.count > 0 {
                        yearData.removeAll()
                    }
                    for year in data{
                        let data = Double(Int(year["value"].stringValue)!)
                        yearData.append(data)
                    }
                    if yearAxis.count > 0 {
                        yearAxis.removeAll()
                    }
                    for axis in Xaxis{
                        let data = axis["value"].stringValue
                        yearAxis.append(data)
                    }
                case "month":
                    if monthData.count > 0 {
                        monthData.removeAll()
                    }
                    for year in data{
                        let data = Double(Int(year["value"].stringValue)!)
                        monthData.append(data)
                    }
                    if monthAxis.count > 0 {
                        monthAxis.removeAll()
                    }
                    for axis in Xaxis{
                        let data = axis["value"].stringValue
                        monthAxis.append(data)
                    }
                default:
                    print("Hello")
                }
             
               
            }

            }else {
                 MsgCount["processing"] = json["processing"].stringValue
                 MsgCount["pending"] = json["pending"].stringValue
                 MsgCount["complete"] = json["complete"].stringValue
            }
            
            self.tableView.reloadData()
        }catch let error {
            print(error.localizedDescription)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return dayAxis.count > 0 ? 1 : 0
        default:
            return 3
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "salesAnalytics") as? ced_salesAnalyticscell{
                cell.LineView.shouldAdaptRange = true
                cell.LineView.setData(dayData, withLabels: dayAxis)
                cell.ChangeAction.setTitle("Today", for: .normal)
                cell.ChangeAction.addTarget(self, action: #selector(self.changeLineData(sender:)), for: .touchUpInside)
                return cell
            }
        default:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "mageOpenStatusCell") as? mageOpenStatusCell {
                cell.label.backgroundColor = UIColor(hexString: appData.themeColor)
                switch indexPath.row {
                case 0:
                    cell.label.text = "Pending Orders" + " (\(MsgCount["pending"] ?? "0"))"
                case 1:
                    cell.label.text = "Processing Orders" + " (\( MsgCount["processing"] ?? "0"))"
                    
                default:
                    cell.label.text = "Complete Orders" + " (\( MsgCount["complete"] ?? "0"))"
                    
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    @objc func changeLineData(sender:UIButton){
        let dropDown = DropDown();
        dropDown.dataSource = ["Today","Week","Month","Year"];
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: UIControl.State());
            self.changeLineChart(item)
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    
    func changeLineChart(_ title:String){
        
        let indexpath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexpath) as? ced_salesAnalyticscell
    
        if(title == "Today"){
            if(dayData.count != 0 ){
                cell!.LineView.setData(dayData, withLabels: dayAxis)
            }
        }else if(title == "Week"){
            if(weekData.count != 0 ){
                cell!.LineView.setData(weekData, withLabels: weekAxis)
            }
        }else if(title == "Month"){
            if(monthData.count != 0 ){
                cell!.LineView.setData(monthData, withLabels: monthAxis)
            }
            
        }else if(title == "Year"){
            if(yearData.count != 0 ){
                cell!.LineView.setData(yearData, withLabels: yearAxis)
            }
            
        }
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch  indexPath.section {
        case 0:
            return 300
        default:
            return 50
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            print("Hello")
        default:
            print("Hello")
            if let viewController =  storyboard?.instantiateViewController(withIdentifier: "orderPage") as? OrderDetailViewController {
                switch indexPath.row{
                case 0:
                    viewController.status = "assigned"
                    viewController.titel = "Pending Orders"
                case 1:
                    viewController.status = "out_for_delivery"
                     viewController.titel = "Processing Orders"
                default:
                    viewController.status = "delivered"
                     viewController.titel = "Complete Orders"
                }
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}
