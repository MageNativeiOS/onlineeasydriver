//
//  AppDelegate.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps


let google_Api_Key = "AIzaSyD-pCTcBlyetKWNTcd30CjSZIQcPMsJ9DA"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

     //MARK: - Properties
    var window: UIWindow?
    var locationManager : CLLocationManager!
    var defaults        = UserDefaults.standard


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        if(UserDefaults.standard.bool(forKey: "isLogin")){
           
            let vc=UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderNavigate") as! OrderNavigationController
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
        else
        {
           
            let vc=UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! LoginViewController
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
        
        GMSServices.provideAPIKey("AIzaSyDQvveYRyyX5wNletdKKJ4pt5XlFbW7fWY")
        GMSPlacesClient.provideAPIKey("AIzaSyDQvveYRyyX5wNletdKKJ4pt5XlFbW7fWY")
         
      IQKeyboardManager.shared.enable = true
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        getApiKey()
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        let udid=UIDevice.current.identifierForVendor!.uuidString
        print("UDID")
        print(udid)
        print("token")
        print(deviceTokenString)
        /*let alert=UIAlertController(title: "Token", message: deviceTokenString, preferredStyle: .alert)
        let action=UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            
            
        })
        let action_cancel=UIAlertAction(title: "Cancel", style: .destructive, handler: { (action: UIAlertAction!) in
            return
        })
        alert.addAction(action)
        alert.addAction(action_cancel)
        alert.modalPresentationStyle = .fullScreen;
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)*/
        if(deviceTokenString != "")
        {
            var params = Dictionary<String, String>();
            let reqUrl =  appData.storeUrl+"mobidelivery/setdevice"
            print(reqUrl)
            var postString=Dictionary<String,Dictionary<String,String>>()
            var postString1:NSString=""
            var makeRequest = URLRequest(url: URL(string: reqUrl)!)
            var postData = ""
            if let uniqId = UIDevice.current.identifierForVendor {
                if let _ = UserDefaults.standard.object(forKey: "isLogin"){
                    let userId = UserDefaults.standard.value(forKey: "mageid") as! String
                    params = ["token":deviceTokenString,"driver_id":userId,"type":"1",
                    "unique_id":uniqId.uuidString]
                }
                else{
                    UserDefaults.standard.set(["token":deviceTokenString,
                    "unique_id":uniqId.uuidString], forKey: "deviceToken")
                    return;
                }
                
            }
            if(!params.isEmpty){
                makeRequest.httpMethod = "POST"
                postString=["parameters":[:]]
                for (key,value) in params
                {
                    _ = postString["parameters"]?.updateValue(value, forKey:key)
                }
                
                postString1=postString.convtToJson()
                print(postString)
                makeRequest.httpBody = postString1.data(using: String.Encoding.utf8.rawValue)
                makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
            let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
                // check for http errors
                if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                {
                    DispatchQueue.main.async
                        {
                            
                    }
                    return;
                }
                
                // code to fetch values from response :: start
                
                
                guard error == nil && data != nil else
                {
                    DispatchQueue.main.async
                        {
                            
                    }
                    return
                }
                
                DispatchQueue.main.async
                    {
                        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                        
                        
                }
            })
            
            task.resume()
        }
    }
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        let state = UIApplication.shared.applicationState
        let test=userInfo["aps"] as! Dictionary<String,Any>
        let main=test
        UserDefaults.standard.set(true, forKey: "pending")
        //let data=userInfo["data"] as! Dictionary<String,Any>
        if state == .active {
            
            
            let alert=UIAlertController(title: "New Order", message: main["alert"] as? String, preferredStyle: .alert)
            let action=UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderNavigate") as! OrderNavigationController
                self.window?.rootViewController = vc;
                self.window?.makeKeyAndVisible()
                
            })
            let action_cancel=UIAlertAction(title: "Cancel", style: .destructive, handler: { (action: UIAlertAction!) in
                return
            })
            alert.addAction(action)
            alert.addAction(action_cancel)
            alert.modalPresentationStyle = .fullScreen;
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            return
        }
        else if state == .background{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderNavigate") as! OrderNavigationController
            self.window?.rootViewController = vc;
            self.window?.makeKeyAndVisible()
        }
        else if state == .inactive{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderNavigate") as! OrderNavigationController
            self.window?.rootViewController = vc;
            self.window?.makeKeyAndVisible()
        }
        else{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderNavigate") as! OrderNavigationController
            self.window?.rootViewController = vc;
            self.window?.makeKeyAndVisible()
        }

    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "driverApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

