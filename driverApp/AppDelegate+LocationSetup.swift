//
//  AppDelegate+LocationSetup.swift
//  driverApp
//
//  Created by cedcoss on 26/06/20.
//  Copyright © 2020 Saumya Kashyap. All rights reserved.
//

extension AppDelegate: CLLocationManagerDelegate{

    func setupLocation(){
        locationManager                 = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate        = self
        locationManager.startUpdatingLocation()
    }

    func getApiKey() {
       var request = URLRequest(url: URL(string: "https://onlineasy.in/rest/V1/mobidelivery/metadata")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
         // print(String(data: data, encoding: .utf8)!)
            
            guard let json = try? JSON(data: data ) else { return }
            print(json)
            
            if json["success"].boolValue {
                
                var mapApi = String()
               
                for val in json["config_data"].arrayValue {
                    if val["key"].stringValue == "google_map_api_key" {
                        mapApi = val["value"].stringValue
                        print(mapApi)
                        GMSServices.provideAPIKey(mapApi)
                        self.setupLocation()
                    } else if val["key"] == "display_fee" {
                        UserDefaults.standard.set(val["value"].stringValue, forKey: "displayFee")
                    }
                }
            }
        }

        task.resume()
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //  MARK: - get the location
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        print("Lat==\(location.coordinate.latitude) Long= \(location.coordinate.longitude)")
        //  MARK: - save to userdefault if nil and if already present apply the distance check
        if let _ = defaults.object(forKey: "isLogin"){
            if defaults.value(forKey: "userLocation") as? [String:Double] == nil{
                print("No location in user default")
                saveupdatedUserLocation(location)
                updateLocationToServer(location)

            }else{
                print("Location already present in user default")
                guard let oldLocation = defaults.value(forKey: "userLocation") as? [String:Double] else {return}
                let oldlocationtoCL   = convertToCLLocation(oldLocation)

                let distanceFromOldLocation = oldlocationtoCL.distance(from: location)
                print("DistanceFromOldLocation===",distanceFromOldLocation.rounded())

                if distanceFromOldLocation > 300 {
                    print("Distance is greater than 300 m from old location")
                    saveupdatedUserLocation(location)
                    updateLocationToServer(location)
                }
            }
        }
    }

    func convertToCLLocation(_ latlongDict:[String:Double]) -> CLLocation {
        guard let lat = latlongDict["lat"], let long = latlongDict["long"] else {return .init()}
        return CLLocation(latitude: lat, longitude: long)
    }

    func saveupdatedUserLocation(_ location:CLLocation){
        var loc         = [String:Double]()
        loc["lat"]      = location.coordinate.latitude
        loc["long"]     = location.coordinate.longitude
        defaults.removeObject(forKey: "userLocation")
        defaults.set(loc, forKey: "userLocation")
    }

    func updateLocationToServer(_ location: CLLocation){
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            guard let placemark = placemarks?.first else {
                let errorString = error?.localizedDescription ?? "Unexpected Error"
                print("Unable to reverse geocode the given location. Error: \(errorString)")
                return
            }
            let reversedGeoLocation = ReversedGeoLocation(with: placemark)

            let postString =  [
                "driver_id" : self.defaults.value(forKey: "mageid") as? String,
                "location"  : reversedGeoLocation.formattedAddress,
                "latitude"  : "\(location.coordinate.latitude)",
                "longitude" : "\(location.coordinate.longitude)",
                "city"      : reversedGeoLocation.city,
                "state"     : reversedGeoLocation.state,
                "country"   : reversedGeoLocation.country,
                "zipcode"   : reversedGeoLocation.zipCode
            ]
            self.sendRequest123(url: "mobidelivery/driver/currentLocation/save", params: postString as! Dictionary<String, String>)
        }
    }

    func sendRequest123(url:String,params:Dictionary<String,String>){

        let reqUrl = appData.storeUrl+url
        var postString=Dictionary<String,Dictionary<String,String>>()
        var postString1:NSString=""
        print(url)
        print(reqUrl)
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)

        if(!params.isEmpty){
            makeRequest.httpMethod = "POST"
            postString=["parameters":[:]]
            for (key,value) in params
            {
                _ = postString["parameters"]?.updateValue(value, forKey:key)
            }
            postString1=postString.convtToJson()
            makeRequest.httpBody = postString1.data(using: String.Encoding.utf8.rawValue)
            makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }else {
            makeRequest.httpMethod = "GET"
        }
        print(reqUrl)
        print(postString)

        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                DispatchQueue.main.async
                    {
                        // self.sendRequest(url: url,params:params,store:store)
                        print("poststring=\(postString1)")
                        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        print("response = \(response)")
                        print(try? JSON(data: data!))
                     //   self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return;
            }
            // code to fetch values from response :: start
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        print("error=\(error)")
                        print(try? JSON(data: data!))
                       // self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return
            }
            DispatchQueue.main.async
                {
                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                    print(try? JSON(data: data!))
                    //self.recieveResponse(data: data, requestUrl: url, response: response)
            }
        })
        task.resume()
    }
}

