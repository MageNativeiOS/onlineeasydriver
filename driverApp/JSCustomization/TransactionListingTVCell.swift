//
//  TransactionListingTVCell.swift
//  driverApp
//
//  Created by cedcoss on 19/06/20.
//  Copyright © 2020 Saumya Kashyap. All rights reserved.
//

import UIKit

class TransactionListingTVCell: UITableViewCell {


    lazy var mainStack: UIStackView = {
        let stackView                                        = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        stackView.distribution                               = .fillEqually
        stackView.spacing                                    = 2
        stackView.alignment                                  = .fill
        stackView.axis                                       = .vertical
        return stackView
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    func setupView(){
        self.addSubview(mainStack)
        mainStack.topAnchor.constraint(equalTo: self.topAnchor,constant: 8).isActive = true
        mainStack.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 8).isActive = true
        mainStack.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -8).isActive = true
        mainStack.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -8).isActive = true
    }

    var headerLabelStyle = [UILabel]()

    var records: Record?{
        didSet{
            guard let orderId = records?.orderId,let amount = records?.amount,let shipmentId = records?.shipmentId,let orderNumer = records?.orderNumber else {return}

            let orderNumberHeader = UILabel()
            orderNumberHeader.text = " ORDER NUMBER"
            mainStack.addArrangedSubview(orderNumberHeader)

            let label1 = UILabel()
            label1.text = " \(orderId)"
            mainStack.addArrangedSubview(label1)

            let amountHeader = UILabel()
            amountHeader.text = " AMOUNT"
            mainStack.addArrangedSubview(amountHeader)

            let label2 = UILabel()
            label2.text = " \(amount)"
            mainStack.addArrangedSubview(label2)

            let shipmentIdHeader = UILabel()
            shipmentIdHeader.text = " SHIPMENT ID"
            mainStack.addArrangedSubview(shipmentIdHeader)

            let label3 = UILabel()
            label3.text = " \(shipmentId)"
            mainStack.addArrangedSubview(label3)

            let orderNumerHeader = UILabel()
            orderNumerHeader.text = " ORDER NUMBER"
            mainStack.addArrangedSubview(orderNumerHeader)

            let label4 = UILabel()
            label4.text = " \(orderNumer)"
            mainStack.addArrangedSubview(label4)


            headerLabelStyle.append(orderNumberHeader)
            headerLabelStyle.append(amountHeader)
            headerLabelStyle.append(shipmentIdHeader)
            headerLabelStyle.append(orderNumerHeader)

            for label in headerLabelStyle{
                label.backgroundColor = .lightGray
                label.layer.cornerRadius = 5
            }
        }
    }
}
