//
//  PayRequestListingVC.swift
//  driverApp
//
//  Created by cedcoss on 20/06/20.
//  Copyright © 2020 Saumya Kashyap. All rights reserved.
//

import UIKit

class PayRequestListingVC: BaseViewController {

    var payRequestListingModel: TransactionListingModel?{
        didSet{
            tableView.reloadData()
        }
    }

    var driverID: String? = {
        return UserDefaults.standard.value(forKey: "mageid") as? String
    }()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
         makeNetworkCall()
        // Do any additional setup after loading the view.
    }

    func setupTable(){
           tableView.delegate      = self
           tableView.dataSource    = self
        tableView.register(TransactionListingTVCell.self, forCellReuseIdentifier: "TransactionListingTVCell")
       }

    func makeNetworkCall(_ page:String = "1"){
        let postString =  ["page":page,"driver_id":driverID]
        self.sendRequest(url: "mobideliverytransaction/list", params: postString as! Dictionary<String, String>)
    }

    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        do{
            guard let json = try? JSON(data:data!),let data = data else {return}
            print(json)
            if json["success"].boolValue == true{
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.payRequestListingModel = try decoder.decode(TransactionListingModel.self, from: data)
                    print("====",self.payRequestListingModel as Any)
                }else{
                    print("Error")
                }
            }
        catch
        {
            print("catchedError")
        }
    }
}


extension PayRequestListingVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return payRequestListingModel?.records?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionListingTVCell", for: indexPath) as! TransactionListingTVCell
        cell.setupView()
        cell.records = payRequestListingModel?.records?[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
}

extension PayRequestListingVC: UITableViewDelegate{

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
}
