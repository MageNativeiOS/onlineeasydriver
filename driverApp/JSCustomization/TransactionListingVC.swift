//
//  TransactionlistingVC.swift
//  driverApp
//
//  Created by cedcoss on 19/06/20.
//  Copyright © 2020 Saumya Kashyap. All rights reserved.
//

import UIKit

class TransactionListingVC: BaseViewController {

    var transactionListingModel: TransactionListingModel?{
        didSet{
            tableView.reloadData()
        }
    }

    var driverID: String? = {
        return UserDefaults.standard.value(forKey: "mageid") as? String
    }()

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        makeNetworkCall()
    }

    func makeNetworkCall(_ page:String = "1"){
        let postString =  ["page":page,"driver_id":driverID]
        self.sendRequest(url: "mobideliverytransaction/list", params: postString as! Dictionary<String, String>)
    }

    func setupTable(){
        tableView.delegate      = self
        tableView.dataSource    = self
        tableView.register(TransactionListingTVCell.self, forCellReuseIdentifier: "TransactionListingTVCell")
    }

    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        do{
            guard let json = try? JSON(data:data!),let data = data else {return}
            print(json)
            if json["success"].boolValue == true{
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    self.transactionListingModel = try decoder.decode(TransactionListingModel.self, from: data)
                    print("====",self.transactionListingModel as Any)
                }else{
                    print("Error")
                }
            }
        catch
        {
            print("catchedError")
        }
    }
}

extension TransactionListingVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionListingModel?.records?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionListingTVCell", for: indexPath) as! TransactionListingTVCell
        cell.setupView()
        cell.records = transactionListingModel?.records?[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset) <= 40 {
            print("fffwfwfwf")
//            page += 1
//            print(page)
//            print(self.getRatingFeeds?.lastPage)
//
//            if page > self.getRatingFeeds?.lastPage ?? 1 {
//                self.view.showmsg(msg: "No more reviews to show")
//                return
//            }
//            loadReviewData(of: String(page))
        }
    }
}

extension TransactionListingVC: UITableViewDelegate{

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
}
