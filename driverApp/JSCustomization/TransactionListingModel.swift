//
//  TransactionListingModel.swift
//  driverApp
//
//  Created by cedcoss on 19/06/20.
//  Copyright © 2020 Saumya Kashyap. All rights reserved.
//

import Foundation

// MARK: - TransactionListingModel
struct TransactionListingModel: Codable {
    let success: Bool?
    let message: String?
    let records: [Record]?
}

// MARK: - Record
struct Record: Codable {
    let success: Bool?
    let message: String?
    let orderId: Int?
    let orderNumber: String?
    let shipmentId: Int?
    let shipmentIncrementID: String?
    let id, driverId, orderAssignmentId, amount: Int?
    let baseAmount: Int?
    let currencyCode, baseCurrencyCode, paymentMethod, paymentDetails: String?
    let status: Int?
    let createdAt, updatedAt: String?
}
