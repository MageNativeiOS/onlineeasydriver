//
//  orderCell.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit
protocol acceptOrder {
    func acceptOrderClicked(index:Int)
    func rejectOrder(index: Int)
}
class orderCell: UIView {
    
    
    @IBOutlet var wrapperView: UIView!
    
    @IBOutlet weak var rejectButton: UIButton!
    
    @IBOutlet weak var acceptOrder: UIButton!
    @IBOutlet weak var mainVIewContainer: UIView!
    @IBOutlet weak var orderNum: UILabel!
    @IBOutlet weak var orderPlacedDate: UILabel!
    @IBOutlet weak var arrow: UIButton!
    @IBOutlet weak var assignTo: UILabel!
    @IBOutlet weak var viewDetails: UIButton!
    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var shipTo: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var orderDescView: UIView!
    @IBOutlet weak var orderDescViewHeight: NSLayoutConstraint!
    var delegate:acceptOrder?
    
    var view:UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        //mainVIewContainer.backgroundColor=UIColor(hexString: appData.themeColor)
        mainVIewContainer.backgroundColor=UIColor.white
        arrow.tintColor=UIColor(hexString: appData.themeColor)
        viewDetails.setTitleColor(UIColor(hexString: appData.themeColor2), for: .normal)
        mainVIewContainer.makeCard(mainVIewContainer, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4)
        wrapperView.makeCard(wrapperView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4)
        orderNum.textColor=UIColor(hexString: appData.themeColor2)
        orderId.textColor=UIColor.gray
        amount.textColor=UIColor.gray
        shipTo.textColor=UIColor.gray
        status.textColor=UIColor.gray
        orderPlacedDate.textColor=UIColor.gray
        acceptOrder.addTarget(self, action: #selector(self.acceptOrderButton(sender:)), for: .touchUpInside)
        rejectButton.addTarget(self, action: #selector(rejectClicked(sender:)), for: .touchUpInside)
    }
    
    @objc func acceptOrderButton(sender:UIButton){
        delegate?.acceptOrderClicked(index: viewDetails.tag)
    }
    
    @objc func rejectClicked(sender: UIButton){
        delegate?.rejectOrder(index: viewDetails.tag)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "orderCell", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
   

}
