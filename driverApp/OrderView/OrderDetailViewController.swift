//
//  OrderDetailViewController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class OrderDetailViewController: BaseViewController,acceptOrder{
   
    @IBOutlet weak var noOrderImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var stackView: UIStackView!
   // @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mainViewWidth: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    var fromHome = false
    var status = String()
    var page = 1
    let screenSize: CGRect = UIScreen.main.bounds
    var titel = String()
    var heightToUse : CGFloat = 10
    var user_id = UserDefaults.standard.value(forKey: "mageid")
    var userData = [String:String]()
   
    var orderdata=[JSON]()
    var checkImage=0
    
    var orderListing = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if let userId = UserDefaults.standard.value(forKey: "mageid") as? String {
            let params = ["driver_id":userId]
            self.sendRequest(url: "mobidelivery/customer/driverprofile", params: params)
        }
       
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        cedMageLoaders.addDefaultLoader(me: self)
        loadOrderData()
    }
    
   @objc func arrowChanged(_ sender:UIButton)
    {
        if sender.currentImage == UIImage(named: "downArrow")
        {
            print("down")
            checkImage=1
            sender.setImage(UIImage(named: "upArrow"), for: .normal)
        }
        else{
            print("up")
            checkImage=2
            sender.setImage(UIImage(named: "downArrow"), for: .normal)
        }
    }
    @objc func doTap(_ sender:UIButton)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "viewOrder") as! ViewOrderDetailsController
        if status.lowercased() == "assigned".lowercased(){
            vc.isPendingOrder = true
        }
        vc.order_id = orderListing[sender.tag]["order_id"] ?? ""
        vc.getAssignID = orderListing[sender.tag]["assign_id"] ?? ""
        vc.shipment_id = orderListing[sender.tag]["shipment_id"] ?? ""
        
        navigationController?.pushViewController(vc, animated: true)
    }
    func loadOrderData(){
        if orderListing.count > 0 {
            page = page + 1
        }
        var postString = ["user_id":user_id]
        var url = "mobidelivery/customer/orderlisting"
        if status != "" {
            url = "mobidelivery/customer/dashboard/orders"
            postString = ["order_status":status,"driver_id":user_id,"page":"\(page)"]
        }
        self.sendRequest(url: url, params: postString as! Dictionary<String, String>)
    }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do{
            if let json = try? JSON(data:data)
            {
                print(json)
                
                let json = json[0]
                
                if requestUrl == "mobidelivery/customer/deliveryorderacpt" {
                    if json["success"].stringValue == "true"{
                        self.view.showToastMsg(json["message"].stringValue)
                        self.status = "out_for_delivery"
                        self.page = 1
                        self.titel = "Processing Orders"
                        orderdata.removeAll()
                        orderListing.removeAll()
                        cedMageLoaders.addDefaultLoader(me: self)
                        self.loadOrderData()
                    }else {
                        self.showAlert(title: "Error", msg: json["message"].stringValue)
                    }
                }else if(requestUrl == "mobidelivery/customer/driverprofile"){
                if json["success"].stringValue == "true" {
                    userData["name"] = json["name"].stringValue
                    userData["email"] = json["email"].stringValue
                    userData["status"] = json["status"].stringValue
                
                }else{
                    print("Hello")
                    }
                }else {
                if json["data"]["status"].stringValue=="success" || json["data"]["success"].stringValue == "true"
                {
//                    if orderListing.count > 0 {
//                        orderListing.removeAll()
//                    }
                    if  json["data"]["orderdata"] != nil
                    {
                        orderdata=json["data"]["orderdata"].array!
                        if orderdata.count > 0 {
                            for i in orderdata
                            {
                                var temp = [String:String]()
                            
                            if let order_statu = i["order_status"].string
                            {
                                temp["order_status"]=order_statu
                            }
                        
                            temp["shipment_status_label"] = i["shipment_status_label"].stringValue
                           temp["order_id"]=i["order_id"].stringValue
                            temp["shipment_id"]=i["shipment_id"].stringValue
                            temp["shipment_increment_id"] = i["shipment_increment_id"].stringValue
                                temp["assign_id"] = i["assign_id"].stringValue
                            if let ship_t = i["ship_to"].string
                            {
                               temp["ship_to"]=ship_t
                            }
                            if let numbe=i["number"].string
                            {
                                temp["number"]=numbe
                            }
                            if let dat=i["date"].string
                            {
                                temp["date"]=dat
                            }
                            if let total_amoun=i["total_amount"].string
                            {
                               temp["total_amount"]=total_amoun
                            }
                            if let assignto=i["assign_to"].string
                            {
                                  temp["assign_to"]=assignto
                            }
                                orderListing.append(temp)
                            }
                        }
                        loadArrayView()
                    }
                    loadOrderData()
                }
                if json["data"]["success"].stringValue=="false"
                {
                    if orderListing.count == 0 {
                    self.view.showToastMsg("No Order Placed.")
                    scrollView.isHidden=true
                    noOrderImage.image=UIImage(named: "No-order")
                    }
                }
                }
            }
        }
        catch
        {
            print("ErrorOccured")
        }
        cedMageLoaders.removeLoadingIndicator(me: self)
    }
    func loadArrayView()
    {
        if mainView.subviews.count > 0 {
            mainView.subviews.forEach {
                $0.removeFromSuperview()
            }
        }
        mainViewWidth.constant = scrollView.frame.width
        let label = UILabel()
        label.frame = CGRect(x: 10, y: 5, width: mainViewWidth.constant-20,height: CGFloat(50))
        label.text = titel
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = UIColor(hexString: appData.themeColor)
        mainView.addSubview(label)
        
       
        mainViewHeight.constant = 50
        for (key,order) in orderListing.enumerated()
        {
            let orderView=orderCell()
            
            orderView.tag=122344
            orderView.autoresizingMask = [UIView.AutoresizingMask.flexibleLeftMargin,UIView.AutoresizingMask.flexibleRightMargin];
            var height = CGFloat(260)
            if self.status == "assigned" {
                orderView.acceptOrder.isHidden = false
                orderView.rejectButton.isHidden = false;
                height = CGFloat(311)
            }else {
                orderView.acceptOrder.isHidden = true
                orderView.acceptOrder.isHidden = true;
            }
            orderView.frame = CGRect(x: 0, y: mainViewHeight.constant+20, width: mainViewWidth.constant-20,height: height)
            orderView.center.x = mainView.center.x
            orderView.orderNum.text = "#" + order["number"]! + " - " +  order["shipment_increment_id"]!
            orderView.orderPlacedDate.text="Placed On "+order["date"]!
          //  orderView.arrow.addTarget(self, action: #selector(arrowChanged(_:)), for: .touchUpInside)
            orderView.amount.text=order["total_amount"]
            orderView.shipTo.text=order["ship_to"]
            orderView.orderId.text=order["order_id"]
            orderView.delegate = self
//            var status = order["order_status"]
//            if self.status != "" {
//                switch self.status {
//                case "assigned":
//                    status = "Pending"
//                case "out_for_delivery":
//                    status = "Out for Delivery"
//                case "delivered":
//                    status = "Delivered"
//                default :
//                    status = "Pending"
//                }
//            }
            orderView.status.text=order["shipment_status_label"]
            orderView.assignTo.textColor=UIColor.orange
            orderView.assignTo.text="ASSIGN TO: "+order["assign_to"]!.uppercased()
            orderView.viewDetails.tag=key
           orderView.viewDetails.addTarget(self, action: #selector(doTap(_:)), for: .touchUpInside)
            mainViewHeight.constant += (height + 20)
            mainView.addSubview(orderView)
        }
    }

    func acceptOrderClicked(index: Int) {
      let order = orderListing[index]
        var params = ["driver_id":user_id,"order_id":order["order_id"],"shipment_id":order["shipment_id"]]
        params["accept_order"] = userData["status"]!
        self.sendRequest(url: "mobidelivery/customer/deliveryorderacpt", params: params as! Dictionary<String, String>)
    }
    
    func rejectOrder(index: Int){
        let order = orderListing[index]
        var params = ["driver_id":user_id,"order_id":order["order_id"],"shipment_id":order["shipment_id"]]
        params["accept_order"] = "0"
        self.sendRequest(url: "mobidelivery/customer/deliveryorderacpt", params: params as! Dictionary<String, String>)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
