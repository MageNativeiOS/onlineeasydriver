//
//  ViewOrderDetailsController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 22/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit
import SDWebImage

class ViewOrderDetailsController: BaseViewController {

    @IBOutlet weak var showRouteMap: UIButton!
    @IBOutlet weak var feedbackButton: UIButton!
    @IBOutlet weak var requestPay: UIButton!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var orderPlaced: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var methodType: UILabel!
    @IBOutlet weak var shipTo: UILabel!
    //@IBOutlet weak var method_title: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var pincode: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var mobileNum: UILabel!
    @IBOutlet weak var shipment: UILabel!
    @IBOutlet weak var shippingMethod: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var taxprice: UILabel!
    @IBOutlet weak var charges: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var grandtotal: UILabel!
    
    @IBOutlet weak var reuestPayHeight: NSLayoutConstraint!
    @IBOutlet weak var mainViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    
    var getAssignID = String()

    
    var user_id = UserDefaults.standard.value(forKey: "mageid")
    var order_id = ""

    var orderView  = JSON()
    var orderData  = [JSON]()
    var imageArray = [String]()
    var nameArray  = [String]()
    var priceArray = [String]()
    var qtyArray=[String]()
    var row_subtotal=[String]()
    var isPendingOrder=Bool()
    var shipment_id = String()

    var showRequestButton = Bool(){
        didSet{
            if showRequestButton{
                reuestPayHeight.constant = 50
            }else{
                reuestPayHeight.constant = 0
            }
        }
    }
    var stringVal = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor=UIColor.white
        
        //cedMageLoaders.addDefaultLoader(me: self)
        loadOrderData()
        feedbackButton.layer.cornerRadius=feedbackButton.frame.width/2
        showRouteMap.layer.cornerRadius = showRouteMap.frame.width/2
        self.view.bringSubviewToFront(feedbackButton)
        self.view.bringSubviewToFront(showRouteMap)
        feedbackButton.backgroundColor=UIColor(hexString: appData.themeColor)
        self.view.bringSubviewToFront(feedbackButton)
        feedbackButton.addTarget(self, action: #selector(feedbackPressed(_:)), for: .touchUpInside)
        price.textColor=UIColor(hexString: appData.themeColor)
        orderPlaced.textColor=UIColor.gray
        methodType.textColor=UIColor.gray
        grandtotal.textColor=UIColor(hexString: appData.themeColor)
        showRouteMap.backgroundColor = UIColor(hexString: appData.themeColor)
        if isPendingOrder {
            feedbackButton.isHidden = true
            showRouteMap.isHidden = true
//            showRouteMap.setTitle("Accept Order", for: .normal)
//            showRouteMap.addTarget(self, action: #selector(self.acceptOrder(sender:)), for: .touchUpInside)
        }else {
            feedbackButton.isHidden = false
            showRouteMap.isHidden = false
            showRouteMap.addTarget(self, action: #selector(self.goToMapView(sender:)), for: .touchUpInside)
        }
        //self.sendRequest(url: "mobidelivery/isActiveSentOtp", params: [:])
        requestPay.addTarget(self, action: #selector(requestPayment(_ :)), for: .touchUpInside)
    }


    @objc func requestPayment(_ sender:UIButton){
        print("Request Payment clicked")
        let params = ["driver_id":user_id,"order_assignment_id":getAssignID]
        self.sendRequest(url: "mobideliverytransaction/requestPay", params: params as! Dictionary<String, String>)
    }

    @objc func acceptOrder(sender:UIButton){
        var params = ["driver_id":user_id,"order_id":order_id,"shipment_id":orderView[0]["shipment_id"].stringValue]
        params["accept_order"] = "1"
        self.sendRequest(url: "mobidelivery/customer/deliveryorderacpt", params: params as! Dictionary<String, String>)
    }

    @objc func feedbackPressed(_ sender:UIButton)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "feedback")as! FeedbackViewController
        vc.order_id = order_id
        vc.topText = orderView[0]["orderlabel"].stringValue
        vc.showPop = stringVal
        vc.shipment_id = orderView[0]["shipment_id"].stringValue
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func loadOrderData(){
        let postString =  ["user_id":user_id,"order_id":order_id,"shipment_id":shipment_id]
        self.sendRequest(url: "mobidelivery/customer/orderview", params: postString as! Dictionary<String, String>)
    }
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        do{
            if requestUrl == "mobidelivery/isActiveSentOtp"{
                if let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                    stringVal = str as String
                }
                return
            }

            if requestUrl == "mobideliverytransaction/requestPay"{
                guard let json = try? JSON(data:data!) else {return}
                print("json==",json)
                self.view.showToastMsg(json["message"].stringValue )
                return
            }

            if let json = try? JSON(data:data!)
            {
                let json = json[0]
                if requestUrl ==  "mobidelivery/customer/deliveryorderacpt" {
                    print(json)
                    if json["success"].stringValue == "true"{
                      self.view.showToastMsg(json["message"].stringValue)
                      self.navigationController?.popViewController(animated: true)
                    }else {
                         self.showAlert(title: "Error", msg: json["message"].stringValue)
                    }
                }  else {
                    
                    print("FinalJSON==",json)
                    print(json["data"]["orderview"])

                    if  json["data"]["orderview"] != nil
                    {
                        //mainViewWidth.constant=self.view.frame.width
                        stackViewHeight.constant=0
                        orderView=json["data"]["orderview"][0]
                        print(orderView.count)
                        //for i in orderView
                       // {
                            
                            orderData=orderView["ordered_items"].arrayValue
                            for i in orderData
                            {
                                if let img = i["product_image"].string
                                {
                                    imageArray.append(img)
                                }
                                if let product_name = i["product_name"].string
                                {
                                    nameArray.append(product_name)
                                }
                                if let product_qty = i["product_qty"].string
                                {
                                    qtyArray.append(product_qty)
                                }
                                if let product_price=i["product_price"].string
                                {
                                    priceArray.append(product_price)
                                }
                                if let rowsubtotal=i["rowsubtotal"].string
                                {
                                    row_subtotal.append(rowsubtotal)
                                }
                            }
                            orderNumber.text=orderView["orderlabel"].stringValue
                            orderPlaced.text="Placed on "+orderView["orderdate"].stringValue
                            price.text=orderView["grandtotal"].stringValue
                            methodType.text="To be paid by "+orderView["method_title"].stringValue
                            shipTo.text=orderView["ship_to"].stringValue
                            street.text=orderView["street"].stringValue
                            city.text=orderView["city"].stringValue
                            country.text=orderView["country"].stringValue
                            mobileNum.text=orderView["mobile"].stringValue
                            pincode.text=orderView["pincode"].stringValue
                            state.text=orderView["state"].stringValue
                            subtotal.text=orderView["subtotal"].stringValue
                            discount.text=orderView["discount"].stringValue
                            taxprice.text=orderView["tax_amount"].stringValue
                            grandtotal.text=orderView["grandtotal"].stringValue
                            charges.text=orderView["shipping"].stringValue
                            showRequestButton = orderView["show_request_btn"].boolValue
                            if orderData.count <= 1
                            {
                                shipment.text="\(orderData.count)"+" item"
                            }
                            else{
                                shipment.text="\(orderData.count)"+" items"
                            }
                            shippingMethod.text=orderView["shipping_method"].stringValue
                        //}
                    }
                    
                   if stackView.subviews.count > 0 {
                        stackView.subviews.forEach{
                            $0.removeFromSuperview()
                        }
                    }
                    for i in 0..<orderData.count
                    {
                        let orderView=viewOrderDetails()
                        orderView.tag=1223445
                        orderView.makeCard(orderView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4)
                        orderView.autoresizingMask = [UIView.AutoresizingMask.flexibleLeftMargin,UIView.AutoresizingMask.flexibleRightMargin];
                        orderView.frame = CGRect(x: 0, y: 0, width: stackView.frame.width-10,height: CGFloat(130))
                        orderView.center.x = self.view.center.x;
                        orderView.amount.text=row_subtotal[i]
                        if imageArray[i] == ""
                        {
                            orderView.productImage.image=UIImage(named: "header")
                        }
                        else{
                            orderView.productImage.sd_setImage(with: URL(string: imageArray[i]))
                        }
                        orderView.productName.text=nameArray[i]
                        orderView.productPrice.text=priceArray[i]
                        orderView.quantity.text = Float(qtyArray[i])?.rounded().description
                        
                        stackView.addArrangedSubview(orderView)
                        stackView.distribution = .fillEqually
                        stackView.spacing=10
                        stackViewHeight.constant += 140
                    }
                    mainViewHeight.constant += stackViewHeight.constant
                }
            }
        }
        catch
        {
            print("catchedError")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func goToMapView(sender:UIButton){
        if let viewcontrol =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "googleMapViewController")  as? googleMapViewController {
            var address = street.text!
            address += " "
            address += city.text!
            address += " "
            address += state.text!
            address += " "
            address += country.text!
                
                
            viewcontrol.address = address//street.text! + " "  + city.text! + " " + state.text! + " " + country.text!
            self.navigationController?.pushViewController(viewcontrol, animated: true)
        }
    }
}
