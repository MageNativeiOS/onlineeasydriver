//
//  OrderNavigationController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 21/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class OrderNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mageOpenDahbaord") as! mageOpenDahbaord
        self.setViewControllers([vc], animated: true)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(hexString: appData.themeColor)]
        //self.navigationBar.tintColor = UIColor(hexString: appData.themeColor)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
