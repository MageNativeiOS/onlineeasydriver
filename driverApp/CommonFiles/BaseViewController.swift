//
//  BaseViewController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        let navigationTitle = UIImageView(frame: CGRect(x: 0, y: 0, width: 110, height: 40))
        navigationTitle.image = UIImage(named: "headerLogo")
        navigationTitle.contentMode = .scaleAspectFit
        self.navigationItem.title   = "Online Easy Driver App"
        self.navigationController?.navigationBar.tintColor = UIColor(hexString: appData.themeColor)
          self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backArrow")
        self.navigationController?.navigationBar.barTintColor = .white
        if #available(iOS 13.0, *) {
            navigationController?.navigationBar.standardAppearance.titleTextAttributes = [.foregroundColor: UIColor(hexString: appData.themeColor)!]
        } else {
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(hexString: appData.themeColor)!]
        }
        let tempButton=UIButton(type: .custom)
        tempButton.frame=CGRect(x: 0, y: 0, width: 30, height: 30)
        tempButton.setBackgroundImage(UIImage(named: "profileHome")?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempButton.tintColor = UIColor(hexString: appData.themeColor)
        tempButton.contentMode = .scaleToFill
        tempButton.addTarget(self, action: #selector(gotoProfile), for: .touchUpInside)
        let logoutButton = UIBarButtonItem(customView: tempButton)
        self.navigationItem.rightBarButtonItems=[logoutButton]
        
    }
    
    @objc func gotoProfile(){
        if let viewControl = storyboard?.instantiateViewController(withIdentifier: "userAccount") as? mageOpenAccountController {
            self.navigationController?.pushViewController(viewControl, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func sendRequest(url:String,params:Dictionary<String,String>){
        
        cedMageLoaders.addDefaultLoader(me: self)
        
        let reqUrl = appData.storeUrl+url
        
        var postString=Dictionary<String,Dictionary<String,String>>()
        var postString1:NSString=""
        print(url)
        print(reqUrl)
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)

        if(!params.isEmpty){
            makeRequest.httpMethod = "POST"
            postString=["parameters":[:]]
            for (key,value) in params
            {
                _ = postString["parameters"]?.updateValue(value, forKey:key)
            }
            
            postString1=postString.convtToJson()
            makeRequest.httpBody = postString1.data(using: String.Encoding.utf8.rawValue)
            makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }else {
            makeRequest.httpMethod = "GET"
        }
        
        print(reqUrl)
        print(postString)
       
        //makeRequest.setValue("mobiconnectsecrethash", forHTTPHeaderField: "Mobiconnectheader")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        // self.sendRequest(url: url,params:params,store:store)
                        print("poststring=\(postString1)")
                        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                        self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        print("error=\(error)")
                        self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return
            }
            
            DispatchQueue.main.async
                {
                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    
            }
        })
        
        task.resume()
    }
    
    func recieveResponse(data:Data?,requestUrl:String?,response:URLResponse?){
        
    }
    /* func recieveResponse(requestUrl:String,data:Data){
     
     }*/
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension Dictionary{
    func convtToJson() -> NSString {
        do {
            let json = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return NSString(data: json, encoding: String.Encoding.utf8.rawValue)!
        }catch {
            return ""
        }
    }
}
extension UIViewController{
    public func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx);
        return emailTest.evaluate(with: testStr);
        
    }
    public func isValidName(testStr:String) -> Bool
    {
        let RegEx = "[a-zA-Z ]*$";
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx);
        return Test.evaluate(with: testStr);
        
    }
    func showAlert(title: String, msg: String){
        let alert=UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action=UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        return
    }
}

extension UIColor {
    public convenience init?(hexString: String) {
        var cString:String = hexString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines as NSCharacterSet as CharacterSet).uppercased();
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }
        if ((cString.count) != 6) {
            return nil;
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        let r = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0;
        let g = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0;
        let b = CGFloat(rgbValue & 0x0000FF) / 255.0;
        let a =  CGFloat(1.0);
        self.init(red: r, green: g, blue: b, alpha: a)
        return
    }
}
