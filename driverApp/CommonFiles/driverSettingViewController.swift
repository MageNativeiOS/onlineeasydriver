//
//  driverSettingViewController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//




import Foundation

final class appData {
//    static let storeUrl = "https://www.uae.conektr.com/rest/V1/"//"https://www.dev.conektr.com/rest/V1/"//"https://www.uae.conektr.com/rest/V1/"
    //OriginalBaseUrl=="https://demo.cedcommerce.com/magento4/admin-mobidelivery2.3.4/rest/V1/"

    static let storeUrl     = "https://onlineasy.in/rest/V1/"
    //static let storeUrl     = "https://demo.cedcommerce.com/magento4/admin-mobidelivery2.3.4/rest/V1/"
    static let themeColor   = "#559824"
    static let themeColor2  = "#559824"
    static let shared = appData()
    static var displayFee: Bool {
        guard let check = UserDefaults.standard.value(forKey: "displayFee") as? String else { return false }
        return check == "false" ? false : true
    }
}

extension Dictionary{
    func convertToJson() -> NSString {
        do {
            let json = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return NSString(data: json, encoding: String.Encoding.utf8.rawValue)!
        }catch {
            return ""
        }
    }
}
