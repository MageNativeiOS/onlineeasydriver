//
//  LoginViewController.swift
//  driverApp
//
//  Created by Saumya Kashyap on 19/01/18.
//  Copyright © 2018 Saumya Kashyap. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var userName: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var loginButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        userName.placeholderColor=UIColor.white
        userName.textColor=UIColor.white
        userName.lineColor=UIColor.white
        userName.titleColor = UIColor.white
        userName.selectedTitleColor = UIColor.white
        userName.selectedLineColor = UIColor.white
        
        
        
        password.placeholderColor=UIColor.white
        password.textColor=UIColor.white
        password.lineColor=UIColor.white
        password.titleColor = UIColor.white
        password.selectedTitleColor = UIColor.white
        password.selectedLineColor = UIColor.white
        
        loginButton.layer.cornerRadius = 5
        loginButton.addTarget(self, action: #selector(loginButtonPressed(_:)), for: .touchUpInside)
        loginButton.backgroundColor=UIColor.white
        loginButton.setTitleColor(UIColor.black, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    @objc func loginButtonPressed(_ sender:UIButton)
    {
        if userName.text == "" || password.text == ""
        {
            showAlert(title: "", msg: "UserName And Password is Required")
        }
        else
        {
            print("hello")
            var postString = [String:String]()
            postString = ["email":userName.text!,"password":password.text!]

        self.sendRequest(url:"mobidelivery/customer/login",params:postString)
        }
    }
    
    override func recieveResponse(data:Data?,requestUrl:String?,response:URLResponse?) {
        guard let data = data else {return}
        do{
            if let json = try? JSON(data:data)
            {
                print(json)
                print("__________")
                let jsonData=json[0]["data"]
                let customerData=jsonData["customer"]
                if customerData[0]["status"].stringValue == "success"
                {
                    print("true")
                    let customerDetails = customerData[0]["details"]
                    let user_id = customerDetails["user_id"].stringValue
                    let user_name = customerDetails["user_name"].stringValue
                    UserDefaults.standard.set(true, forKey: "isLogin")
                    UserDefaults.standard.set(user_name, forKey: "mageusername")
                    UserDefaults.standard.set(user_id, forKey: "mageid")
                    self.setDeviceToken()
                    //UserDefaults.standard.set("success", forKey: "checkloginstatus")
                    print(user_id)
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderNavigate") as! OrderNavigationController
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                    
                }else {
                    showAlert(title: "", msg: "UserName And Password is invalid")
                }
            }
        }
        catch
        {
            print("ErrorOccured")
        }
    }
    
    func setDeviceToken(){
        if let deviceToken = UserDefaults.standard.value(forKey: "deviceToken") as? [String:String]{
            var postString=Dictionary<String,Dictionary<String,String>>()
            var postString1:NSString=""
            var params = [String:String]()
            let reqUrl =  appData.storeUrl+"mobidelivery/setdevice"
            var makeRequest = URLRequest(url: URL(string: reqUrl)!)
            var postData = ""
            if let uniqId = UIDevice.current.identifierForVendor {
                if let _ = UserDefaults.standard.object(forKey: "isLogin"){
                    let userId = UserDefaults.standard.value(forKey: "mageid") as! String
                    params = ["token":deviceToken["token"]!,"driver_id":userId,"type":"1",
                    "unique_id":uniqId.uuidString]
                }
                else{
                    UserDefaults.standard.set(["token":deviceToken["token"],
                    "unique_id":uniqId.uuidString], forKey: "deviceToken")
                    return;
                }
            }
            if(!params.isEmpty){
                makeRequest.httpMethod = "POST"
                postString=["parameters":[:]]
                for (key,value) in params
                {
                    _ = postString["parameters"]?.updateValue(value, forKey:key)
                }
                
                postString1=postString.convtToJson()
                makeRequest.httpBody = postString1.data(using: String.Encoding.utf8.rawValue)
                makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
            let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
                // check for http errors
                if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                {
                    DispatchQueue.main.async
                        {
                            
                    }
                    return;
                }
                
                // code to fetch values from response :: start
                guard error == nil && data != nil else
                {
                    DispatchQueue.main.async
                        {
                            
                    }
                    return
                }
                
                DispatchQueue.main.async
                    {
                        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                        
                        
                }
            })
            
            task.resume()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden=false
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.userName.inputAccessoryView = keyboardToolbar
        self.password.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
